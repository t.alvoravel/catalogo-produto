package com.challenge.product.controller;

import com.challenge.product.domain.dto.ProductDto;
import com.challenge.product.domain.mapper.ProductMapper;
import com.challenge.product.domain.request.ProductRequest;
import com.challenge.product.domain.response.ProductResponse;
import com.challenge.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.net.URI;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(path = ProductController.PRODUCT)
public class ProductController {

    public static final String PRODUCT = "/products";
    public static final String PATH_ID = "/{id}";
    public static final String SEARCH = "/search";

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductMapper productMapper;

    @GetMapping
    public ResponseEntity<List<ProductResponse>> getAllProducts(){
        List<ProductResponse> productResponseList = productService.findAllProducts().stream()
                .map(product -> productMapper.dtoToResponse(productMapper.entityToDto(product)))
                .collect(toList());

        return ResponseEntity.ok(productResponseList);
    }

    @GetMapping(PATH_ID)
    public ResponseEntity<ProductResponse> getProduct(@PathVariable @Valid String id) {
        return productService.findProductById(Long.valueOf(id))
                .map(product -> ResponseEntity.ok(productMapper.dtoToResponse(productMapper.entityToDto(product))))
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(SEARCH)
    public ResponseEntity<List<ProductResponse>> getFilterProducts(@RequestParam (name = "q", required = false) String name,
                                                                   @RequestParam (name = "min_price", required = false) BigDecimal minPrice,
                                                                   @RequestParam (name = "max_price", required = false) BigDecimal maxPrice) {

        List<ProductResponse> productResponseList = productService.findFilterProducts(name, minPrice, maxPrice)
                .stream().map(product -> productMapper.dtoToResponse(productMapper.entityToDto(product)))
                .collect(toList());

        return ResponseEntity.ok(productResponseList);
    }

    @PostMapping
    @Transactional
    public ResponseEntity<ProductResponse> insertProduct(@RequestBody @Valid ProductRequest productRequest,
                                                         UriComponentsBuilder uriComponentsBuilder) {
        ProductDto productDto = productMapper.entityToDto(productService.insertProduct(productMapper.requestToEntity(productRequest)));
        URI uri = uriComponentsBuilder.path(PRODUCT.concat(PATH_ID)).buildAndExpand(productDto.getId()).toUri();
        return ResponseEntity.created(uri).body(productMapper.dtoToResponse(productDto));
    }

    @PutMapping(PATH_ID)
    @Transactional
    public ResponseEntity<ProductResponse> updateProduct(@RequestBody @Valid ProductRequest productRequest,
                                                         @PathVariable @Valid String id) {
        return productService.findProductById(Long.valueOf(id))
                .map(product -> {
                    ProductDto productDto = productService.updateProduct(Long.valueOf(id), productRequest);
                    return ResponseEntity.ok(productMapper.dtoToResponse(productDto));
                })
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping(PATH_ID)
    @Transactional
    public ResponseEntity<Object> removeProduct(@PathVariable @Valid String id) {
        return productService.findProductById(Long.valueOf(id))
                .map(product -> {
                    productService.deleteProduct(Long.valueOf(id));
                    return ResponseEntity.ok().build();
                })
                .orElse(ResponseEntity.notFound().build());
    }

}
