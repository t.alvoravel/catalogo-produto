package com.challenge.product.handler;

public class ValidationErrorDto {

    private int statusCode;
    private String message;

    public ValidationErrorDto(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }
}
