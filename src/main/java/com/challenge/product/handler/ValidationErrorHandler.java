package com.challenge.product.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static java.util.Objects.nonNull;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestControllerAdvice
public class ValidationErrorHandler {

    @Autowired
    private MessageSource messageSource;

    @ResponseStatus(code = BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationErrorDto> handlerMethodArgumentNotValidException(MethodArgumentNotValidException exception){
        FieldError fieldError = exception.getBindingResult().getFieldError();
        String message = null;

        if(nonNull(fieldError)) {
            message = new StringBuilder()
                    .append(fieldError.getField())
                    .append(": ")
                    .append(messageSource.getMessage(fieldError, LocaleContextHolder.getLocale()))
                    .toString();
        }

        ValidationErrorDto validationErrorDto = new ValidationErrorDto(BAD_REQUEST.value(), message);

        return new ResponseEntity<>(validationErrorDto, BAD_REQUEST);
    }

}
