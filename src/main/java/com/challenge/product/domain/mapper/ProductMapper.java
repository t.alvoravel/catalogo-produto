package com.challenge.product.domain.mapper;

import com.challenge.product.domain.dto.ProductDto;
import com.challenge.product.domain.request.ProductRequest;
import com.challenge.product.domain.response.ProductResponse;
import com.challenge.product.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDto entityToDto(Product product);

    Product dtoToEntity(ProductDto productDto);

    Product requestToEntity(ProductRequest productRequest);

    ProductResponse dtoToResponse(ProductDto productDto);

    void updateProductFromRequest(ProductRequest productRequest, @MappingTarget Product product);

}
