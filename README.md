# Catalogo de Produtos

## Instruções Para Rodar o Projeto

1. Ter instalado o Java versão 11
2. Baixar o .jar nesse [link](https://gitlab.com/t.alvoravel/catalogo-produto/-/raw/master/target/product-1.0.0.jar?inline=false)
3. Rodar o seguinte comando no diretório o qual está o .jar baixado no passo anterior: 
`java -jar <NOME_DO_.JAR>` 
