package com.challenge.product.domain.mapper;

import com.challenge.product.domain.dto.ProductDto;
import com.challenge.product.domain.request.ProductRequest;
import com.challenge.product.domain.response.ProductResponse;
import com.challenge.product.entity.Product;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-03-22T08:53:02-0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.8 (AdoptOpenJDK)"
)
@Component
public class ProductMapperImpl implements ProductMapper {

    @Override
    public ProductDto entityToDto(Product product) {
        if ( product == null ) {
            return null;
        }

        ProductDto productDto = new ProductDto();

        productDto.setId( product.getId() );
        productDto.setName( product.getName() );
        productDto.setDescription( product.getDescription() );
        productDto.setPrice( product.getPrice() );

        return productDto;
    }

    @Override
    public Product dtoToEntity(ProductDto productDto) {
        if ( productDto == null ) {
            return null;
        }

        Product product = new Product();

        product.setId( productDto.getId() );
        product.setName( productDto.getName() );
        product.setDescription( productDto.getDescription() );
        product.setPrice( productDto.getPrice() );

        return product;
    }

    @Override
    public Product requestToEntity(ProductRequest productRequest) {
        if ( productRequest == null ) {
            return null;
        }

        Product product = new Product();

        product.setName( productRequest.getName() );
        product.setDescription( productRequest.getDescription() );
        product.setPrice( productRequest.getPrice() );

        return product;
    }

    @Override
    public ProductResponse dtoToResponse(ProductDto productDto) {
        if ( productDto == null ) {
            return null;
        }

        ProductResponse productResponse = new ProductResponse();

        if ( productDto.getId() != null ) {
            productResponse.setId( String.valueOf( productDto.getId() ) );
        }
        productResponse.setName( productDto.getName() );
        productResponse.setDescription( productDto.getDescription() );
        productResponse.setPrice( productDto.getPrice() );

        return productResponse;
    }

    @Override
    public void updateProductFromRequest(ProductRequest productRequest, Product product) {
        if ( productRequest == null ) {
            return;
        }

        product.setName( productRequest.getName() );
        product.setDescription( productRequest.getDescription() );
        product.setPrice( productRequest.getPrice() );
    }
}
