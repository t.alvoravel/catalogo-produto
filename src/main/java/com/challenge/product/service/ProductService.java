package com.challenge.product.service;

import com.challenge.product.domain.dto.ProductDto;
import com.challenge.product.domain.mapper.ProductMapper;
import com.challenge.product.domain.request.ProductRequest;
import com.challenge.product.entity.Product;
import com.challenge.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;
import static org.springframework.data.domain.ExampleMatcher.NullHandler.IGNORE;
import static org.springframework.data.domain.ExampleMatcher.StringMatcher.CONTAINING;
import static org.springframework.data.domain.ExampleMatcher.matchingAny;
import static org.springframework.data.jpa.convert.QueryByExamplePredicateBuilder.getPredicate;

@Service
@Transactional(value = "transactionManager")
public class ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductRepository productRepository;

    public Optional<Product> findProductById(Long id) {
        return productRepository.findById(id);
    }

    public List<Product> findAllProducts() {
        return productRepository.findAll();
    }

    public List<Product> findFilterProducts(String q, BigDecimal minPrice, BigDecimal maxPrice) {
        return productRepository.findAll(getSpecification(minPrice, maxPrice, q, getProductExample()));
    }

    private Example<Product> getProductExample() {
        ExampleMatcher matcher = matchingAny()
                .withNullHandler(IGNORE)
                .withIgnoreCase()
                .withStringMatcher(CONTAINING)
                .withIgnoreNullValues();

        return Example.of(new Product(), matcher);
    }

    private Specification<Product> getSpecification(
            BigDecimal minPrice, BigDecimal maxPrice, String q, Example<Product> product){
        return (root, query, builder) -> {
            final List<Predicate> predicateList = new ArrayList<>();

            if(nonNull(minPrice)) {
                predicateList.add(builder.greaterThanOrEqualTo(root.get("price"), minPrice));
            }

            if(nonNull(maxPrice)) {
                predicateList.add(builder.lessThanOrEqualTo(root.get("price"), maxPrice));
            }

            if(nonNull(q)){
                String partner = new StringBuilder("%").append(q).append("%").toString();
                predicateList.add(builder.or(
                        builder.like(root.get("name"), partner),
                        builder.like(root.get("description"), partner)));
            }

            predicateList.add(getPredicate(root, builder, product));

            return builder.and(predicateList.toArray(new Predicate[predicateList.size()]));
        };
    }

    public Product insertProduct(Product product) {
        return productRepository.saveAndFlush(product);
    }

    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }

    public ProductDto updateProduct(Long id, ProductRequest productRequest) {
        Product product = productRepository.getOne(id);
        productMapper.updateProductFromRequest(productRequest, product);
        return productMapper.entityToDto(product);
    }

}
